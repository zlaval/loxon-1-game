package com.loxon.javachallenge.memory;

import com.loxon.javachallenge.memory.api.Game;
import com.loxon.javachallenge.memory.api.GameImpl;

public class GameImplementationFactory {
    public static Game get() {
        return new GameImpl();
    }
}
