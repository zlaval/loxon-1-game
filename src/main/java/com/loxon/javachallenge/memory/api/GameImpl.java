package com.loxon.javachallenge.memory.api;

import com.loxon.javachallenge.memory.api.communication.general.Command;
import com.loxon.javachallenge.memory.api.communication.general.Response;
import one.util.streamex.EntryStream;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GameImpl implements Game {

    private static final int BLOCK_SIZE = 4;

    private List<Player> players = new ArrayList<>();

    private Map<Integer, Memory> memory;

    private int gameRounds;

    private int actualRound;

    @Override
    public Player registerPlayer(String name) {
        Player player = new Player(name);
        players.add(player);
        return player;
    }

    @Override
    public void startGame(List<MemoryState> initialMemory, int rounds) {
        this.memory = transformToMemory(initialMemory);
        this.gameRounds = rounds;
    }

    private Map<Integer, Memory> transformToMemory(List<MemoryState> memoryStates) {
        return EntryStream.of(memoryStates)
                .mapValues(m -> Memory.builder().memoryState(m).build())
                .toSortedMap();
    }

    @Override
    public List<Response> nextRound(Command... requests) {
        actualRound++;
        int roundsLeft = gameRounds - actualRound;
        CommandProcessor commandProcessor = new CommandProcessor(memory, players, roundsLeft);
        return commandProcessor.processCommands(requests);
    }

    @Override
    public List<PlayerScore> getScores() {
        ScoreCalculator scoreCalculator = new ScoreCalculator(memory);
        return scoreCalculator.calculateScores();
    }

    @Override
    public String visualize() {
        return EntryStream.of(memory)
                .mapKeyValue(this::visualizeCell)
                .joining();
    }

    private String visualizeCell(Integer index, Memory memory) {
        StringBuilder sb = new StringBuilder();
        if (index % BLOCK_SIZE == 0) {
            sb.append(System.lineSeparator());
            for (int i = 0; i < 47; i++) {
                sb.append("-");
            }
            sb.append(System.lineSeparator());
        }
        sb.append("[[");
        String memoryState = memory.getMemoryState().toString();
        memoryState.replace("OWNED_", "O");
        sb.append(memoryState, 0, 3);
        sb.append("||");
        if (Objects.nonNull(memory.getPlayer())) {
            String name = memory.getPlayer().getName();
            if (name.length() > 3) {
                name = name.substring(0, 2);
            } else {
                for (int i = 0; i < 3 - name.length(); i++) {
                    name += " ";
                }
            }
            sb.append(name.toUpperCase());
        } else {
            sb.append("NO");
        }
        sb.append("]]");
        if (index % BLOCK_SIZE != 3) {
            sb.append(" ");
        }
        return sb.toString();
    }
}
