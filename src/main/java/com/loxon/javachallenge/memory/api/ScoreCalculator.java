package com.loxon.javachallenge.memory.api;

import one.util.streamex.EntryStream;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ScoreCalculator {

    private Map<Integer, Memory> memory;

    private Map<Player, PlayerScore> scores = new HashMap<>();

    private Player blockOwner;
    private int ownedCellInBlock;

    public ScoreCalculator(Map<Integer, Memory> memory) {
        this.memory = memory;
    }

    public List<PlayerScore> calculateScores() {
        for (Map.Entry<Integer, Memory> memoryCell : memory.entrySet()) {
            processMemoryCell(memoryCell.getKey(), memoryCell.getValue());
        }
        return EntryStream.of(scores).values().toList();
    }

    private void processMemoryCell(Integer memoryAddress, Memory memoryCell) {
        Player player = memoryCell.getPlayer();
        PlayerScore playerScore = null;
        if (Objects.nonNull(player)) {
            playerScore = createOrGetScore(player);
            calculateScore(memoryCell, playerScore);
        }
        checkBlock(player, memoryAddress, playerScore);
    }

    private void calculateScore(Memory memoryCell, PlayerScore playerScore) {
        switch (memoryCell.getMemoryState()) {
            case FORTIFIED:
                playerScore.setFortifiedCells(playerScore.getFortifiedCells() + 1);
            case ALLOCATED:
                playerScore.setOwnedCells(playerScore.getOwnedCells() + 1);
                playerScore.setTotalScore(playerScore.getTotalScore() + 1);
                break;
        }
    }

    private void checkBlock(Player actualPlayer, Integer memoryAddress, PlayerScore playerScore) {
        if (Objects.nonNull(actualPlayer) && !actualPlayer.equals(blockOwner)) {
            resetBlockData();
        }
        if (memoryAddress % 4 == 0 || Objects.isNull(actualPlayer)) {
            resetBlockData();
        }
        if (Objects.nonNull(actualPlayer)) {
            setBlockOwner(actualPlayer);
            handleBlockCount(playerScore);
        }
        if (memoryAddress % 4 == 3) {
            resetBlockData();
        }
    }

    private void handleBlockCount(PlayerScore playerScore) {
        ownedCellInBlock++;
        checkFullBlockAllocated(playerScore);
    }

    private void checkFullBlockAllocated(PlayerScore playerScore) {
        if (ownedCellInBlock == 4) {
            playerScore.setOwnedBlocks(playerScore.getOwnedBlocks() + 1);
            playerScore.setTotalScore(playerScore.getTotalScore() + 4);
        }
    }

    private void setBlockOwner(Player actualPlayer) {
        blockOwner = actualPlayer;
    }

    private void resetBlockData() {
        blockOwner = null;
        ownedCellInBlock = 0;
    }

    private PlayerScore createOrGetScore(Player player) {
        if (!scores.containsKey(player)) {
            PlayerScore playerScore = new PlayerScore(player);
            scores.put(player, playerScore);
        }
        return scores.get(player);
    }

}
