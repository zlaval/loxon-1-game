package com.loxon.javachallenge.memory.api;


import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Memory {
    private Player player;
    private MemoryState memoryState;

    public Memory(MemoryState memoryState) {
        this.memoryState = memoryState;
    }
}
