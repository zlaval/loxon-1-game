package com.loxon.javachallenge.memory.api;

import com.loxon.javachallenge.memory.api.communication.commands.*;
import com.loxon.javachallenge.memory.api.communication.general.Command;
import com.loxon.javachallenge.memory.api.communication.general.CommandGeneral;
import com.loxon.javachallenge.memory.api.communication.general.Response;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CommandProcessor {

    private List<Player> players;
    private Map<Integer, Memory> memory;
    private Set<Player> processedPlayer = new HashSet<>();
    private Set<Integer> modifiedCells = new HashSet<>();
    private int roundsLeft;

    public CommandProcessor(Map<Integer, Memory> memory, List<Player> players, int roundsLeft) {
        this.memory = memory;
        this.roundsLeft = roundsLeft;
        this.players = players;
    }

    public List<Response> processCommands(Command... commands) {
        List<Response> responses = new ArrayList<>();
        setCorruptedField(commands);
        for (Command command : commands) {
            if (players.contains(command.getPlayer()) && !processedPlayer.contains(command.getPlayer())) {
                Response response = processCommand(command);
                responses.add(response);
                processedPlayer.add(command.getPlayer());
            }
        }
        return responses;
    }

    private void setCorruptedField(Command... commands) {
        Map<Integer, Long> fieldWithModifyRequests = Arrays.stream(commands)
                .filter(c -> isTypeOf(c, CommandGeneral.class))
                .<CommandGeneral>map(this::cast)
                .map(CommandGeneral::getCells)
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        fieldWithModifyRequests.forEach((k, v) -> {
            if (!isCellFinalOrNull(k) && v > 1) {
                setCorrupt(k);
                setCellModified(k);
            }
        });
    }

    private void setCorrupt(Integer... cells) {
        for (Integer cell : cells) {
            if (!isCellFinalOrNull(cell)) {
                memory.get(cell).setMemoryState(MemoryState.CORRUPT);
                memory.get(cell).setPlayer(null);
            }
        }
    }

    private Response processCommand(Command command) {
        Response response = null;
        if (validatePlayerAlreadyTakeAction(command) || validateTooMuchCell(command)) {
            response = errorResponse(command.getPlayer());
        } else {
            response = processCommandByType(command);
        }
        return response;
    }

    private boolean validateTooMuchCell(Command command) {
        if (isTypeOf(command, CommandGeneral.class)) {
            CommandGeneral commandGeneral = cast(command);
            if (commandGeneral.getCells().size() > 2) {
                return true;
            }
            for (Integer cell : commandGeneral.getCells()) {
                if (invalidMemoryAddress(cell)) {
                    return true;
                }
            }
        }
        return false;

    }

    private boolean invalidMemoryAddress(Integer cell) {
        if (Objects.nonNull(cell)) {
            if (cell < 0 || cell >= memory.size()) {
                return true;
            }
        }
        return false;
    }

    private Response errorResponse(Player player) {
        return new ResponseSuccessList(player, Collections.emptyList());
    }

    private Response processCommandByType(Command command) {
        Response response = null;
        if (isTypeOf(command, CommandAllocate.class)) {
            response = allocate(cast(command));
        } else if (isTypeOf(command, CommandFortify.class)) {
            response = forty(cast(command));
        } else if (isTypeOf(command, CommandFree.class)) {
            response = free(cast(command));
        } else if (isTypeOf(command, CommandRecover.class)) {
            response = recover(cast(command));
        } else if (isTypeOf(command, CommandScan.class)) {
            response = scan(cast(command));
        } else if (isTypeOf(command, CommandStats.class)) {
            response = stats(cast(command));
        } else if (isTypeOf(command, CommandSwap.class)) {
            response = swap(cast(command));
        }
        return response;
    }

    private boolean validatePlayerAlreadyTakeAction(Command command) {
        return processedPlayer.contains(command.getPlayer());
    }

    private Response allocate(CommandAllocate allocate) {
        Player player = allocate.getPlayer();
        List<Integer> cells = allocate.getCells();
        if (!isInSameBlock(cells.get(0), cells.get(1))) {
            return errorResponse(player);
        }
        List<Integer> success = new ArrayList<>();
        for (Integer cell : cells) {
            if (!isCellFinalOrNull(cell) && !isCellModified(cell) && !isCorrupt(cell)) {
                Memory memoryCell = memory.get(cell);
                if (memoryCell.getMemoryState() == MemoryState.ALLOCATED) {
                    setCorrupt(cell);
                } else {
                    memoryCell.setPlayer(player);
                    memoryCell.setMemoryState(MemoryState.ALLOCATED);
                    success.add(cell);
                }
                setCellModified(cell);
            }
        }

        return createResponse(player, success);
    }

    private Response forty(CommandFortify fortify) {
        List<Integer> cells = fortify.getCells();
        List<Integer> success = new ArrayList<>();
        for (Integer cell : cells) {
            if (!isCellFinalOrNull(cell) && !isCellModified(cell)) {
                Memory memoryCell = memory.get(cell);
                if (memoryCell.getMemoryState() == MemoryState.ALLOCATED) {
                    memoryCell.setMemoryState(MemoryState.FORTIFIED);
                    success.add(cell);
                }
            }
        }
        return createResponse(fortify.getPlayer(), success);
    }


    private Response free(CommandFree free) {
        List<Integer> cells = free.getCells();
        List<Integer> success = new ArrayList<>();
        for (Integer cell : cells) {
            if (!isCellFinalOrNull(cell) && !isCellModified(cell)) {
                Memory memoryCell = memory.get(cell);
                memoryCell.setMemoryState(MemoryState.FREE);
                memoryCell.setPlayer(null);
                success.add(cell);
                setCellModified(cell);
            }
        }

        return createResponse(free.getPlayer(), success);
    }

    private Response recover(CommandRecover recover) {
        List<Integer> cells = recover.getCells();
        List<Integer> success = new ArrayList<>();
        for (Integer cell : cells) {
            if (!isCellFinalOrNull(cell) && !isCellModified(cell)) {
                Memory memoryCell = memory.get(cell);
                if (memoryCell.getMemoryState() == MemoryState.CORRUPT) {
                    memoryCell.setMemoryState(MemoryState.ALLOCATED);
                    memoryCell.setPlayer(recover.getPlayer());
                    success.add(cell);
                } else {
                    setCorrupt(cell);
                }
                setCellModified(cell);
            }
        }
        return createResponse(recover.getPlayer(), success);
    }

    private Response scan(CommandScan scan) {
        Player player = scan.getPlayer();
        Integer cell = scan.getCell();
        int blockStart = cell - (cell % 4);
        List<MemoryState> memoryStates = new ArrayList<>(4);
        if (invalidMemoryAddress(cell)) {
            blockStart = -1;
        } else {
            for (int i = blockStart; i < blockStart + 4; i++) {
                Memory memoryCell = memory.get(i);
                MemoryState memoryState = getState(player, memoryCell.getMemoryState(), memoryCell.getPlayer());
                memoryStates.add(memoryState);
            }
        }
        return new ResponseScan(player, blockStart, memoryStates);
    }

    private Response stats(CommandStats stats) {
        Player player = stats.getPlayer();
        int ownedCells = 0;
        int freeCells = 0;
        int allocatedCells = 0;
        int corruptCells = 0;
        int fortifiedCells = 0;
        int systemCells = 0;

        for (Memory cell : memory.values()) {
            if (player.equals(cell.getPlayer())) {
                ownedCells++;
            }
            MemoryState state = cell.getMemoryState();
            switch (state) {
                case FORTIFIED:
                    fortifiedCells++;
                    break;
                case ALLOCATED:
                    allocatedCells++;
                    break;
                case FREE:
                    freeCells++;
                    break;
                case SYSTEM:
                    systemCells++;
                    break;
                case CORRUPT:
                    corruptCells++;
                    break;
                default:
                    break;
            }
        }

        return buildStatResponse(player, ownedCells, freeCells, allocatedCells, corruptCells, fortifiedCells, systemCells);
    }

    private Response swap(CommandSwap swap) {
        Player player = swap.getPlayer();
        List<Integer> cells = swap.getCells();
        if (cells.size() < 2) {
            return errorResponse(player);
        }
        Integer firstCell = cells.get(0);
        Integer secondCell = cells.get(1);
        if (isCellFinalOrNull(firstCell) || isCellFinalOrNull(secondCell) || firstCell.equals(secondCell)) {
            return errorResponse(player);
        }
        if (isCellModified(firstCell) || isCellModified(secondCell)) {
            setCorrupt(firstCell, secondCell);
            return errorResponse(player);
        }

        Memory first = memory.get(firstCell);
        Memory second = memory.get(secondCell);
        Player tmp = first.getPlayer();
        first.setPlayer(second.getPlayer());
        second.setPlayer(tmp);
        MemoryState tmpState = first.getMemoryState();
        first.setMemoryState(second.getMemoryState());
        second.setMemoryState(tmpState);
        setCellsModified(cells);
        return createResponse(player, firstCell, secondCell);
    }

    private ResponseSuccessList createResponse(Player player, Integer... cells) {
        List<Integer> cell = Arrays.asList(cells);
        return createResponse(player, cell);
    }

    private ResponseSuccessList createResponse(Player player, List<Integer> cells) {
        return new ResponseSuccessList(player, cells);
    }

    private boolean isAnyCellModified(List<Integer> cells) {
        boolean modified = false;
        for (Integer cell : cells) {
            modified |= isCellModified(cell);
        }
        return modified;
    }

    private boolean isCellModified(Integer cell) {
        return modifiedCells.contains(cell);
    }

    private void setCellsModified(List<Integer> cells) {
        modifiedCells.addAll(cells);
    }

    private void setCellModified(Integer cell) {
        modifiedCells.add(cell);
    }

    private boolean isCellFinalOrNull(Integer cell) {
        if (Objects.isNull(cell)) {
            return true;
        }
        if (memory.containsKey(cell)) {
            MemoryState state = memory.get(cell).getMemoryState();
            if (state == MemoryState.FORTIFIED || state == MemoryState.SYSTEM) {
                return true;
            }
        }
        return false;
    }

    private boolean isCorrupt(Integer cell) {
        if (memory.containsKey(cell)) {
            MemoryState state = memory.get(cell).getMemoryState();
            if (state == MemoryState.CORRUPT) {
                return true;
            }
        }
        return false;
    }


    private ResponseStats buildStatResponse(Player player, int ownedCells, int freeCells, int allocatedCells,
                                            int corruptCells, int fortifiedCells, int systemCells) {
        ResponseStats responseStats = new ResponseStats(player);
        responseStats.setCellCount(memory.size());
        responseStats.setOwnedCells(ownedCells);
        responseStats.setFreeCells(freeCells);
        responseStats.setAllocatedCells(allocatedCells);
        responseStats.setCorruptCells(corruptCells);
        responseStats.setFortifiedCells(fortifiedCells);
        responseStats.setSystemCells(systemCells);
        responseStats.setRemainingRounds(roundsLeft);
        return responseStats;
    }

    private <T> boolean isTypeOf(Command command, Class<T> type) {
        return type.isInstance(command);
    }

    private <T> T cast(Command command) {
        return (T) command;
    }

    private MemoryState getState(Player player, MemoryState memoryState, Player owner) {
        if (memoryState.equals(MemoryState.ALLOCATED) && player.equals(owner)) {
            return MemoryState.OWNED_ALLOCATED;
        } else if (memoryState.equals(MemoryState.FORTIFIED) && player.equals(owner)) {
            return MemoryState.OWNED_FORTIFIED;
        }
        return memoryState;
    }

    private boolean isInSameBlock(Integer a, Integer b) {
        if (Objects.isNull(a) || Objects.isNull(b)) {
            return true;
        }
        int aStart = a - (a % 4);
        int bStart = b - (b % 4);
        return aStart == bStart;
    }

}
